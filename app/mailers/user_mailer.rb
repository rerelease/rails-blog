class UserMailer < ActionMailer::Base
  default :from => "karl.puusepp@gmail.com"
  
  def comment_notification(comment)
    @post = comment.post
    @comment = comment
    formatted = "#{@post.name} <#{@post.email}>"
    mail(:to => formatted, :subject => "New comment notification")
  end
end
